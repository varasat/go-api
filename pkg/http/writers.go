package http

import (
	"bytes"
	"encoding/json"
	"net/http"
)

// WriteResponse is used to write custom responses through the api
func WriteResponse(w http.ResponseWriter, res interface{}, status int) {
	buf := new(bytes.Buffer)
	enc := json.NewEncoder(buf)
	enc.SetEscapeHTML(false)
	enc.SetIndent("", "  ")
	err := enc.Encode(&res)
	if err != nil {
		panic(err)
	}

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(status)

	if _, err := w.Write(buf.Bytes()); err != nil {
		panic(err)
	}
}
