package user

import (
	"encoding/json"
	"gorm.io/gorm"
	domain "isa-funds-api/internal/domain"
	domainInterface "isa-funds-api/internal/domain/interfaces"
	"isa-funds-api/internal/repositories"
	"net/http"
)

// UserAddHandler Dependency injector
type UserAddHandler struct {
	DB *gorm.DB
}

func NewUserAddHandler(db *gorm.DB) *UserAddHandler {
	return &UserAddHandler{DB: db}
}

func (c *UserAddHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	// Check if the appropriate method is used
	if r.Method != http.MethodPost {
		w.WriteHeader(http.StatusMethodNotAllowed)
		w.Write([]byte("Method not allowed"))
		return
	}

	var user domainInterface.User
	var employee domain.Employee

	err := json.NewDecoder(r.Body).Decode(&employee)
	if err == nil {
		user = &employee
	} else {
		var customer domain.Customer
		err = json.NewDecoder(r.Body).Decode(&customer)
	}

	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte("Bad request"))
		return
	}

	userData := repositories.ToUserDbModel(user)
	err = userData.SaveData(c.DB)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte("Something went wrong"))
		return
	}

	w.WriteHeader(http.StatusCreated)
	w.Write([]byte("Success"))
}
