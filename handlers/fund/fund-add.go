package fund

import (
	"encoding/json"
	"gorm.io/gorm"
	"isa-funds-api/internal/db-models"
	"isa-funds-api/internal/domain"
	"net/http"
)

// FundAddHandler Dependency injector
type FundAddHandler struct {
	DB *gorm.DB
}

func NewFundAddHandler(db *gorm.DB) *FundAddHandler {
	return &FundAddHandler{DB: db}
}

func (c *FundAddHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	// Check if the appropriate method is used
	if r.Method != http.MethodPost {
		w.WriteHeader(http.StatusMethodNotAllowed)
		w.Write([]byte("Method not allowed"))
		return
	}

	var fund domain.Fund
	err := json.NewDecoder(r.Body).Decode(&fund)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte("Bad request"))
		return
	}

	companyData := models.ToFundDbModel(fund)
	err = companyData.SaveData(c.DB)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte("Something went wrong"))
		return
	}

	w.WriteHeader(http.StatusCreated)
	w.Write([]byte("Success"))
}
