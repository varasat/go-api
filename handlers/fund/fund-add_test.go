package fund

import (
	"bytes"
	"encoding/json"
	models "isa-funds-api/internal/db-models"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/stretchr/testify/assert"
	"gorm.io/driver/postgres" // Import the PostgreSQL driver for GORM.
	"gorm.io/gorm"
	"isa-funds-api/internal/domain"
)

func TestFundAddHandler_ServeHTTP(t *testing.T) {
	// Set up a PostgreSQL database connection for testing.
	dsn := "host=localhost user=myuser password=mypassword dbname=customer-db port=5432 sslmode=disable"
	db, err := gorm.Open(postgres.Open(dsn), &gorm.Config{})
	if err != nil {
		t.Fatal("Failed to open database:", err)
	}
	// Migrate the database to create the necessary table.
	err = db.AutoMigrate(&models.FundData{})
	if err != nil {
		return
	}

	// Create a FundAddHandler with the test database.
	handler := NewFundAddHandler(db)

	// Create a test HTTP request with a JSON payload.
	fund := domain.Fund{Name: "Test Fund"}
	payload, err := json.Marshal(fund)
	if err != nil {
		t.Fatal("Failed to marshal JSON:", err)
	}
	req, err := http.NewRequest(http.MethodPost, "/fund/add", bytes.NewReader(payload))
	if err != nil {
		t.Fatal("Failed to create request:", err)
	}

	// Create a response recorder to capture the response.
	rr := httptest.NewRecorder()

	// Call the handler's ServeHTTP method.
	handler.ServeHTTP(rr, req)

	// Check the response status code and body.
	assert.Equal(t, http.StatusCreated, rr.Code)
	assert.Equal(t, "Success", rr.Body.String())

	// Optionally, you can check the database to verify that the data was saved correctly.
	var savedFund models.FundData
	if err := db.First(&savedFund).Error; err != nil {
		t.Fatal("Failed to query the database:", err)
	}
	assert.Equal(t, "Test Fund", savedFund.Name)
}
