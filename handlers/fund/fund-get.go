package fund

import (
	"gorm.io/gorm"
	"isa-funds-api/internal/db-models"
	http2 "isa-funds-api/pkg/http"
	"net/http"
)

type FundGetHandler struct {
	DB *gorm.DB
}

func NewFundGetHandler(db *gorm.DB) *FundGetHandler {
	return &FundGetHandler{DB: db}
}

func (c *FundGetHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	if r.Method != http.MethodGet {
		w.WriteHeader(http.StatusMethodNotAllowed)
		w.Write([]byte("Method not allowed"))
		return
	}

	fundId := r.URL.Query().Get("fund_id")

	var err error
	var notFoundMessage string

	if fundId == "" {
		var results []models.FundData
		err = c.DB.Find(&results).Error

		//we're checking if err == nil rather than err != nil so that we can have both error cases resolve in
		//the same place so as to avoid rewriting the reponse code bit like 4 times
		if err == nil {
			companies := models.FundResponse{Funds: results}
			http2.WriteResponse(w, companies, http.StatusOK)
			return
		}

		if err == gorm.ErrRecordNotFound {
			notFoundMessage = "No companies found. Add one? "
		}

	} else {
		fund, err := models.GetFundDataFromId(fundId, c.DB)
		if err == nil {
			http2.WriteResponse(w, fund, http.StatusOK)
			return
		}

		if err == gorm.ErrRecordNotFound {
			notFoundMessage = "Fund not found"
		}

	}

	if err == gorm.ErrRecordNotFound {
		w.WriteHeader(http.StatusNotFound)
		w.Write([]byte(notFoundMessage))
		return
	}

	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte("Something went wrong"))
		return
	}
}
