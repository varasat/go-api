package handlers

import (
	"net/http"
)

type HandlerInterface interface {
	ServeHTTP(w http.ResponseWriter, r *http.Request)
}
