package investment

import (
	"encoding/json"
	"gorm.io/gorm"
	models "isa-funds-api/internal/db-models"
	"isa-funds-api/internal/domain"
	"isa-funds-api/internal/repositories"
	"net/http"
)

// InvestmentAddHandler Dependency injector
type InvestmentAddHandler struct {
	DB *gorm.DB
}

func NewInvestmentAddHandler(db *gorm.DB) *InvestmentAddHandler {
	return &InvestmentAddHandler{DB: db}
}

func (c *InvestmentAddHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	if r.Method != http.MethodPost {
		w.WriteHeader(http.StatusMethodNotAllowed)
		w.Write([]byte("Method not allowed"))
		return
	}
	//todo: add logic to only add new investments to users that have
	var investment domain.Investment
	err := json.NewDecoder(r.Body).Decode(&investment)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte("Bad request"))
		return
	}

	investmentData := models.ToInvestmentDbModel(investment)
	canHaveMoreInvestment, err := repositories.UserCanHaveMoreInvestment(investmentData, c.DB)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte("Something went wrong"))
		return
	}

	if !canHaveMoreInvestment {
		w.WriteHeader(http.StatusUnauthorized)
		w.Write([]byte("This user is not allowed to make further investments"))
		return
	}

	err = investmentData.SaveData(c.DB)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte("Something went wrong"))
		return
	}

	w.WriteHeader(http.StatusCreated)
	w.Write([]byte("Success"))
}
