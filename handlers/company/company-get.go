package company

import (
	"gorm.io/gorm"
	"isa-funds-api/internal/db-models"
	http2 "isa-funds-api/pkg/http"
	"net/http"
)

type CompanyGetHandler struct {
	DB *gorm.DB
}

func NewCompanyGetHandler(db *gorm.DB) *CompanyGetHandler {
	return &CompanyGetHandler{DB: db}
}

func (c *CompanyGetHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	if r.Method != http.MethodGet {
		w.WriteHeader(http.StatusMethodNotAllowed)
		w.Write([]byte("Method not allowed"))
		return
	}

	companyId := r.URL.Query().Get("company_id")

	var err error
	var notFoundMessage string

	if companyId == "" {
		var results []models.CompanyData
		err = c.DB.Find(&results).Error

		//we're checking if err == nil rather than err != nil so that we can have both error cases resolve in
		//the same place so as to avoid rewriting the reponse code bit like 4 times
		if err == nil {
			companies := models.CompanyResponse{Companies: results}
			http2.WriteResponse(w, companies, http.StatusOK)
			return
		}

		if err == gorm.ErrRecordNotFound {
			notFoundMessage = "No companies found. Add one? "
		}

	} else {
		company, err := models.GetCompanyDataFromId(companyId, c.DB)
		if err == nil {
			http2.WriteResponse(w, company, http.StatusOK)
			return
		}

		if err == gorm.ErrRecordNotFound {
			notFoundMessage = "Company not found"
		}

	}

	if err == gorm.ErrRecordNotFound {
		w.WriteHeader(http.StatusNotFound)
		w.Write([]byte(notFoundMessage))
		return
	}

	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte("Something went wrong"))
		return
	}
}
