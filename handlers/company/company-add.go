package company

import (
	"encoding/json"
	"gorm.io/gorm"
	"isa-funds-api/internal/db-models"
	"isa-funds-api/internal/domain"
	"net/http"
)

// CompanyAddHandler Dependency injector
type CompanyAddHandler struct {
	DB *gorm.DB
}

func NewCompanyAddHandler(db *gorm.DB) *CompanyAddHandler {
	return &CompanyAddHandler{DB: db}
}

func (c *CompanyAddHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	// Check if the appropriate method is used
	if r.Method != http.MethodPost {
		w.WriteHeader(http.StatusMethodNotAllowed)
		w.Write([]byte("Method not allowed"))
		return
	}

	var company domain.Company
	err := json.NewDecoder(r.Body).Decode(&company)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte("Bad request"))
		return
	}

	companyData := models.ToCompanyDbModel(company)
	err = companyData.SaveData(c.DB)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte("Something went wrong"))
		return
	}

	w.WriteHeader(http.StatusCreated)
	w.Write([]byte("Success"))
}
