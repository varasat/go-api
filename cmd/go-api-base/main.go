package main

import (
	"fmt"
	"gorm.io/driver/postgres"
	"gorm.io/gorm"
	"isa-funds-api/api"
	"log"
)

func main() {
	db, err := gorm.Open(postgres.New(postgres.Config{
		DSN: "host=db user=myuser password=mypassword dbname=customer-db port=5432 sslmode=disable",
	}), &gorm.Config{})
	if err != nil {
		log.Fatal("Failed to connect to database:", err)
	}

	//eventual todo: use mux instead to have a url like user/get/{user_id} instead of user/get?user_id={user_id}

	fmt.Println("connected to db")
	router := api.InitRouter(db)
	router.InitRoutes()
	fmt.Println("initiate routes")
}
