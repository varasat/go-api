# Use an official Go runtime as the base image
FROM golang:1.18-alpine

# Set the working directory in the container
WORKDIR /app

# Copy go.mod and go.sum files to the container
COPY go.mod go.sum ./

# Download and install Go dependencies
RUN go mod download

# Copy the rest of the application code to the container
COPY . .

# Build the Go application from the /cmd/go-api-base/ folder
RUN go build -o main ./cmd/go-api-base/

# Expose port 8080 to the outside world
EXPOSE 8080

# Command to run the executable
CMD ["./main"]