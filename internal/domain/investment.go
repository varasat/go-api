package domain

type Investment struct {
	UserId string  `json:"user_id"`
	FundId int     `json:"fund_id"`
	Amount float64 `json:"amount"`
}
