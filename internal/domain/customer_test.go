package domain

import (
	"testing"
)

func TestCustomer_GetName(t *testing.T) {
	customer := Customer{Id: "123", Name: "John Doe"}

	// Call the GetName method and check if it returns the expected name.
	expectedName := "John Doe"
	actualName := customer.GetName()
	if actualName != expectedName {
		t.Errorf("Expected name %s, but got %s", expectedName, actualName)
	}
}

func TestCustomer_GetID(t *testing.T) {
	customer := Customer{Id: "123", Name: "John Doe"}

	// Call the GetID method and check if it returns the expected ID.
	expectedID := "123"
	actualID := customer.GetID()
	if actualID != expectedID {
		t.Errorf("Expected ID %s, but got %s", expectedID, actualID)
	}
}

func TestCustomer_GetCompanyId(t *testing.T) {
	customer := Customer{Id: "123", Name: "John Doe"}

	// Call the GetCompanyId method and check if it returns an empty string.
	expectedCompanyID := 0
	actualCompanyID := customer.GetCompanyId()
	if actualCompanyID != expectedCompanyID {
		t.Errorf("Expected company ID %d, but got %d", expectedCompanyID, actualCompanyID)
	}
}

func TestCustomer_SetID(t *testing.T) {
	customer := Customer{Id: "123", Name: "John Doe"}

	// Call the SetID method to change the ID.
	newID := "456"
	customer.SetID(newID)

	// Check if the ID has been updated.
	if customer.Id != newID {
		t.Errorf("Expected ID %s, but got %s", newID, customer.Id)
	}
}
