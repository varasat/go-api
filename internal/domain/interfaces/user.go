package domain

// User Defines a User interface with common methods for both Customers and Employee.
type User interface {
	GetName() string
	GetID() string
	GetCompanyId() int
	SetID(id string)
}
