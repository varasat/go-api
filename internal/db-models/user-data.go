package models

import (
	"github.com/google/uuid"
	"gorm.io/gorm"
	domain "isa-funds-api/internal/domain"
	domainInterface "isa-funds-api/internal/domain/interfaces"
)

type UserData struct {
	ID        *string `gorm:"primary_key" ,json:"id,omitempty"`
	Name      string  `json:"name"`
	CompanyId *int    `json:"company_id"`
}

func (UserData) TableName() string {
	return "users"
}

func (u UserData) SaveData(db *gorm.DB) error {
	newId := uuid.New().String()
	u.ID = &newId
	err := db.Create(&u).Error
	if err != nil {
		return err
	}
	return nil
}

func (u UserData) toDomain() domainInterface.User {
	if *u.CompanyId != 0 {
		return &domain.Employee{
			Id:        *u.ID,
			Name:      u.Name,
			CompanyId: *u.CompanyId,
		}
	}

	return &domain.Customer{
		Id:   *u.ID,
		Name: u.Name,
	}
}
