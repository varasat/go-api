package api

import (
	"gorm.io/gorm"
	"isa-funds-api/handlers"
	"isa-funds-api/handlers/company"
	"isa-funds-api/handlers/fund"
	post "isa-funds-api/handlers/investment"
	"isa-funds-api/handlers/user"
	"log"
	"net/http"
)

type Router struct {
	db *gorm.DB
	//we could add a logger here
}

func InitRouter(DB *gorm.DB) *Router {
	return &Router{
		db: DB,
	}
}

func (r Router) InitRoutes() {
	userAddEndpoint := user.NewUserAddHandler(r.db)
	companyAddEndpoint := company.NewCompanyAddHandler(r.db)
	fundAddEndpoint := fund.NewFundAddHandler(r.db)
	investmentAddEndpoint := post.NewInvestmentAddHandler(r.db)
	userGetEndpoint := user.NewUserGetHandler(r.db)
	companyGetEndpoint := company.NewCompanyGetHandler(r.db)
	fundGetEndpoint := fund.NewFundGetHandler(r.db)

	http.HandleFunc("/health", handlers.HealthHandler)                 // health endpoint for the api
	http.HandleFunc("/user/add", userAddEndpoint.ServeHTTP)            // Creates a basic user
	http.HandleFunc("/user/get", userGetEndpoint.ServeHTTP)            // Gets all user data
	http.HandleFunc("/company/add", companyAddEndpoint.ServeHTTP)      // Add a company to the system
	http.HandleFunc("/company/get", companyGetEndpoint.ServeHTTP)      // retrieves a company's details from its id
	http.HandleFunc("/fund/add", fundAddEndpoint.ServeHTTP)            // adds a fund to the list of funds
	http.HandleFunc("/fund/get", fundGetEndpoint.ServeHTTP)            //gets all the funds
	http.HandleFunc("/user/add/fund", investmentAddEndpoint.ServeHTTP) //creates a link between a fund and a user todo: finish this

	log.Fatal(http.ListenAndServe(":8080", nil))
}
