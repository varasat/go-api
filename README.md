# Coding Challenge Andrei Manolache

Hello This is just a version of the previous repo with beter architecture and better tests

## Getting started

- [ ] To run the project just make sure you have docker installed and at least version 1.18 of Go
- [ ] To run the containerized API you should be able to just run the following command
```
make run
```


## Documentation
Please find the documentation [here](https://gitlab.com/varasat/api-usecase-number-1-fund-and-users/-/blob/main/documentation/Cushon%20documentation.pdf?ref_type=heads)

## How to test
- run the api using `make run`
- Sometimes the API starts before the DB on the first load of the DB despite the `depends_on` clause in the docker_compose file,
just rerun `make run` in those cases
- Use your favourite API testing software like Postman to test the endpoints, I've included a collection in [here](https://gitlab.com/varasat/api-usecase-number-1-fund-and-users/-/tree/main/documentation/Postman%20API%20collection?ref_type=heads)

## Author
Andrei Manolache
## License
[![License: MIT](https://img.shields.io/badge/License-MIT-yellow.svg)](https://opensource.org/licenses/MIT)

## Project status
Active